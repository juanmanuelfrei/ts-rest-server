import cors from 'cors';
import express, { Application } from 'express';

import db from '../db/connection';

import userRoutes from '../routes/usuarios';

class Server {

    private app: Application;
    private port: string;
    private apiPaths = {
        usuarios: '/api/usuarios'
    }

    constructor() {
        this.app = express();
        this.port = process.env.PORT || '8080';
        this.conectarDB();
        this.middlewares();
        this.routes();
    }

    listen() {
        this.app.listen(this.port, () => console.log(`Servidor corriendo en ${this.port}`));
    }

    routes() {
        this.app.use(this.apiPaths.usuarios, userRoutes);
    }

    middlewares() {
        this.app.use(cors({}));

        this.app.use(express.json());

        this.app.use(express.static('public'));
    }

    async conectarDB() {
        try {
            await db.authenticate();
            console.log('Database online');
        } catch (error: any) {
            throw new Error(error);
        }
    }

}

export default Server;