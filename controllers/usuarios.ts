import { Request, Response } from "express";
import { where } from "sequelize/types";
import Usuario from "../models/usuario";


export const getUsuarios = async (req: Request, res: Response) => {

    const usuarios = await Usuario.findAll();

    res.json({
        usuarios
    });
}

export const getUsuario = async (req: Request, res: Response) => {
    const { id } = req.params;

    const usuario = await Usuario.findByPk(id);

    if (!usuario) {
        return res.status(404).json({
            msg: `No existe un usuario con ese ID (${id})`
        });
    }

    res.json({
        usuario
    });
}

export const postUsuario = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        const existeEmail = await Usuario.findOne({
            where: {
                email: body.email
            }
        })
        if (existeEmail) {
            return res.status(404).json({
                msg: `Ya existe un usuario con ese mail (${body.email})`
            });
        }
        const usuario = await Usuario.create(body);

        res.json({
            usuario
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: `shit son`
        });
    }
}

export const putUsuario = async (req: Request, res: Response) => {

    const { body } = req;
    const { id } = req.params;

    try {
        const usuario = await Usuario.findByPk(id);
        if (!usuario) {
            return res.status(404).json({
                msg: `No existe un usuario con ese ID (${id})`
            });
        }
    
        await usuario.update(body);
        res.json({
            usuario
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: `shit son`
        });
    }
}

export const deleteUsuario = async (req: Request, res: Response) => {
    const { id } = req.params;
    const usuario = await Usuario.findByPk(id);

    if (!usuario) {
        return res.status(404).json({
            msg: `No existe un usuario con ese ID (${id})`
        });
    }

    // Eliminacion fisica (registro de la BBDD)
    // await usuario.destroy();

    await usuario.update({estado: false});

    res.json({
        usuario
    })
}
