import { Sequelize } from 'sequelize';

const db = new Sequelize('ts-rest-server', 'root', '', {
    host: 'localhost',
    dialect: 'mariadb'
});

export default db;